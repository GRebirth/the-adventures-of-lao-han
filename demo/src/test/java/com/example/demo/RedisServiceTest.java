package com.example.demo;

/**
 * @description: {description}
 * @version: 0.0.1
 * @author: gong
 * @createTime: 2025-03-10 22:53
 **/
import com.example.demo.Service.RedisService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
public class RedisServiceTest {
    @Autowired
    private RedisService redisService;

    @Test
    public void testRedisOperations() {
        // 设置值
        redisService.setValue("testKey", "testValue");
        // 获取值
        Object value = redisService.getValue("testKey");
        assertEquals("testValue", value);
        System.out.println(value.toString());

//        // 删除键
//        redisService.redisTemplate.delete("testKey");
//        assertNull(redisService.getValue("testKey"));
    }
}