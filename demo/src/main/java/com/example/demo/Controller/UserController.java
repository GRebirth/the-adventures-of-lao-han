package com.example.demo.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    private  static final Logger logger =   LoggerFactory.getLogger(UserController.class);



    @GetMapping(value = {"/", "login"})
    public String login(HttpServletRequest request) {
        logger.info("登录");
        HttpSession session =request.getSession(true);
        session.setAttribute("username","loginUser");

        return "login";
    }
    @PostMapping("/login")
    public String handleLogin(@RequestParam String username, @RequestParam String password, RedirectAttributes redirectAttributes) {
        logger.info("处理登录请求, 用户名: {}", username);

        // 这里可以添加实际的登录验证逻辑
        if ("admin".equals(username) && "password".equals(password)) {
            logger.info("登录成功");
            return "redirect:/index.html";
        } else {
            logger.info("登录失败");
            redirectAttributes.addFlashAttribute("msg", "用户名或密码错误");
            return "redirect:/login";
        }
    }

    @GetMapping("/user/{userId}")
    @ResponseBody
    public String  User(@PathVariable Integer userId){
        System.out.println(userId);
        logger.info(String.valueOf(userId));
        return "ok";
    }
    @GetMapping("/index")
    @ResponseBody
    public String  index(){

        return "index";
    }
    @PostMapping("/index.html")
    @ResponseBody
    public String handlePostIndex() {
        logger.info("POST request to /index.html handled successfully");
        return "POST request to /index.html handled successfully";
    }

}
