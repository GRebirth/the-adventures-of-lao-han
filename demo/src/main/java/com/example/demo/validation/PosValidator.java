package com.example.demo.validation;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.example.demo.validation.annotation.PosCheck;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

/**
 * pos使用状态检查
 *
 */
@Data
@Slf4j
public class PosValidator implements ConstraintValidator<PosCheck, Object> {



//    @Override
//    public void initialize(PosCheck constraintAnnotation) {
//        this.posCheck = constraintAnnotation;
//    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        log.info("SN:{}使用状态检查", JSONUtil.toJsonStr(value));
        String sn = (String) value;
        return true;
        // pos机信息
//        PosInfoEntity pmsPosInfo = SpringUtil.getBean(PosInfoService.class)
//                .findByPosSn(sn);
//        String defaultMessage = BeanUtil.isEmpty(pmsPosInfo) ?
//                StrFormatter.format("终端【{}】未入库", sn)
//                : (PosUseStatusEnum.USED.getCode().equals(pmsPosInfo.getUseStatus()) ?
//                StrFormatter.format("终端【{}】已被他人使用", sn)
//                : (PosUseStatusEnum.NOT_CAN_USED.getCode().equals(pmsPosInfo.getUseStatus()) ?
//                StrFormatter.format("终端【{}】不可使用", sn)
//                : (FlagEnum.FLAG.getFlagNum().equals(pmsPosInfo.getDamageFlag()) ?
//                StrFormatter.format("终端【{}】货损", sn)
//                : null)));
//        return Optional.of(posCheck)
//                .map(PosCheck::message)
//                .filter(StrUtil::isBlank)
//                .flatMap(get -> Optional.ofNullable(defaultMessage))
//                .map(message -> {
//                    context.disableDefaultConstraintViolation();
//                    context.buildConstraintViolationWithTemplate(message)
//                            .addConstraintViolation();
//                    return false;
//                })
//                .orElse(true);
    }
}
