package com.example.demo.validation.annotation;


import com.example.demo.validation.DateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 日期合法性校验
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = DateValidator.class)
public @interface DateCheck {

    String message() default "";

    boolean checkLocal() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
