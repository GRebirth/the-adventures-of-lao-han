package com.example.demo.validation.annotation;

import com.example.demo.validation.PaymentParamValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 支付机构必填参数校验
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PaymentParamValidator.class)
public @interface PayemtParamCheck {

    /**
     * 校验提示
     *
     * @return
     */
    String message();

    /**
     * 要进行检验的支付机构
     *
     * @return
     */


    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}