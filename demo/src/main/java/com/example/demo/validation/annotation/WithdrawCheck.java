package com.example.demo.validation.annotation;

import com.example.demo.validation.WithdrawValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = WithdrawValidator.class)
public @interface WithdrawCheck {

    String message() default "";

    /**
     * 是否是钱包提现
     * true 钱包提现
     * false 余额提现
     * @return
     */
    boolean isWallet() default true;


    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
