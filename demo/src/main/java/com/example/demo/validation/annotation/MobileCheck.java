package com.example.demo.validation.annotation;

import com.example.demo.validation.MobileValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 手机号是否已注册验证
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = MobileValidator.class)
public @interface MobileCheck {
    String message();
    /**
     * 设置注册未注册检查标识
     * 注册时检查填true
     * @return true 已注册 false 未注册
     */
    boolean isCheckRegistered() default true;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}