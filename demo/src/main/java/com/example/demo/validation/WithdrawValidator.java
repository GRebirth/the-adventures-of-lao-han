package com.example.demo.validation;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.example.demo.validation.annotation.WithdrawCheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * 提现校验
 */
public class WithdrawValidator implements ConstraintValidator<WithdrawCheck,Object> {


    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        String withdrawAmount = (String) value;
        BigDecimal amount = new BigDecimal(withdrawAmount);
        return true;
//        //查询商户信息
//        Long userId = UserInfoContext.getUserId();
//        MercAccountEntity account = SpringUtil.getBean(MercAccountService.class).getAccountByMercId(userId);
//
//        String defaultMessage = BeanUtil.isEmpty(account)?"查无账户信息" :
//                        NumberUtil.isLess(amount,BigDecimal.TEN)   ?"提现金额不能小于10元":
//                                withdrawCheck.isWallet()?
//                                        ( NumberUtil.isLess(account.getWalletAmount(),amount)?"提现金额不能大于账户余额":
//                               null):
//                                        ( NumberUtil.isLess(account.getBalance(),amount)?"提现金额不能大于余额":
//                                                null);
//
//        return Optional.of(withdrawCheck)
//                .map(WithdrawCheck::message)
//                .filter(StrUtil::isBlank)
//                .flatMap(get -> Optional.ofNullable(defaultMessage))
//                .map(message -> {
//                    context.disableDefaultConstraintViolation();
//                    context.buildConstraintViolationWithTemplate(message)
//                            .addConstraintViolation();
//                    return false;
//                })
//                .orElse(true);
    }
}
