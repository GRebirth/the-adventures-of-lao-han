package com.example.demo.validation;

import cn.hutool.core.util.ObjectUtil;
import com.example.demo.validation.annotation.DateCheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * 日期合法性校验
 */
public class DateValidator implements ConstraintValidator<DateCheck, Object> {

    DateCheck dateCheck;

    @Override
    public void initialize(DateCheck constraintAnnotation) {
        dateCheck = constraintAnnotation;
    }


    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        if (ObjectUtil.isNull(value) || "长期".equals(value)) {
            return true;
        }
        String dateStr = (String) value;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            try {
                Date date = dateFormat.parse(dateStr);
                if (dateStr.equals(dateFormat.format(date))) {
                    // 校验当前日期应小于该日期
                    if (dateCheck.checkLocal()) {
                        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        if (localDate.isBefore(LocalDate.now())) {
                            setDefaultMessage("身份证有效期已失效", context);
                            return false;
                        }
                    }
                } else {
                    setDefaultMessage("身份证日期格式错误", context);
                    return false;
                }
            } catch (ParseException e) {
                setDefaultMessage("身份证日期格式错误", context);
                return false;
            }
        } catch (Exception e) {
            return true;
        }

        return true;
    }

    /**
     * 设置返回校验信息
     *
     * @param defaultMessage
     * @param context
     */
    public void setDefaultMessage(String defaultMessage, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(defaultMessage)
                .addConstraintViolation();
    }


    public static void main(String[] args) {
        String dateStr = "20231131";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            Date date = dateFormat.parse(dateStr);
            if (dateStr.equals(dateFormat.format(date))) {
                // 校验当前日期应小于该日期
                if (true) {
                    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    if (localDate.isBefore(LocalDate.now())) {
                        // setDefaultMessage("身份证有效期已失效", context);
                        System.out.println("身份证有效期已失效");
                        return;
                    }
                }
            } else {
                //setDefaultMessage("身份证日期不合法", context);
                System.out.println("身份证日期不合法");
                return;
            }
        } catch (ParseException e) {
            //setDefaultMessage("身份证日期不合法", context);
            System.out.println("身份证日期不合法");
            return;
        }
        System.out.println("合法");
    }

}
