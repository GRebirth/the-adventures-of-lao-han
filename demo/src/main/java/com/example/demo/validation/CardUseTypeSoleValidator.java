package com.example.demo.validation;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.example.demo.validation.annotation.CardUseTypeSoleCheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;


public class CardUseTypeSoleValidator implements ConstraintValidator<CardUseTypeSoleCheck,Object> {

//    @Override
//    public void initialize(CardUseTypeSoleCheck constraintAnnotation) {
//        cardUseTypeSoleCheck = constraintAnnotation;
//    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        String cardNo = (String) value;
        if (StrUtil.isEmpty(cardNo)) {
            return false;
        }
        if ("NONEED".equals(cardNo)) {
            return true;
        }
//        Long userId = UserInfoContext.getUserId();
//        MercCardUseTypeEnum useType = cardUseTypeSoleCheck.useType();
//        List<MercCardEntity> cards = SpringUtil.getBean(MercCardService.class).queryMercCardByType(userId, useType);

        return true;
//                CollectionUtil.isEmpty(cards) || cards.stream().noneMatch(card ->cardNo.equals(card.getCardNo()));
    }
}
