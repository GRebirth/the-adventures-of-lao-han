package com.example.demo.validation.annotation;


import com.example.demo.validation.IdCardValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 身份证号唯一性检查
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = IdCardValidator.class)
public @interface IdCardCheck {

    String message();

    /**
     * 已注册检查|未注册检查
     * @return true：已注册检查(如果已注册返回错误message) false：未注册检查(如果未注册返回错误message)
     */
    boolean isCheckRegistered() default true;

    /**
     * 不校验身份证支付机构
     * @return
     */

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
