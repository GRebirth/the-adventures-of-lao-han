package com.example.demo.validation;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;

import com.example.demo.validation.annotation.MercStatusCheck;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;


@Slf4j
public class MercStatusValidator implements ConstraintValidator<MercStatusCheck,Object> {

    private MercStatusCheck mercStatusCheck;

    @Override
    public void initialize(MercStatusCheck constraintAnnotation) {
        mercStatusCheck = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
//        Long userId = UserInfoContext.getUserId();
//
//        if (BeanUtil.isEmpty(userId)) {
//            return false;
//        }
//        MerchantInfoEntity entity = SpringUtil.getBean(MerchantInfoService.class).getById(userId);
//        if (BeanUtil.isEmpty(entity)) {
//            return false;
//        }
//
//        PaymentType[] paymentTypes = mercStatusCheck.invalidPayment();
//        int merchantType = mercStatusCheck.merchantType();
//        PaymentType currentPayment = PaymentTypeContainer.getPaymentType();
//        boolean r1 = Arrays.stream(paymentTypes).anyMatch(en -> en.equals(currentPayment));
//        if (r1) {
//            if (merchantType == 0) {
//                //所有商户均放过
//                return true;
//            } else if (merchantType == 1) {
//                //云闪付未绑机商户放过
//                if (StrUtil.startWith(entity.getPosSn(), "YL")) {
//                    return true;
//                }
//            }
//        }
//
//        MercStatusEnum[] statusEnums = mercStatusCheck.mercStatus();
//        //对比商户状态是否在的statusEnums
//        return Arrays.stream(statusEnums)
//                .anyMatch(en -> en.getCode() == entity.getMercStatus());
//    }
        return true;
}}
