package com.example.demo.validation.annotation;

import com.example.demo.validation.BankCardValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = BankCardValidator.class)
public @interface BankCardCheck {

    String message() default "";

    /**
     * 检查储蓄卡/检查信用卡 默认为true
     *
     * @return true: 检查是否为储蓄卡 false: 检查是否为信用卡
     */
    boolean isCheckDebit() default true;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}