package com.example.demo.validation.annotation;

import com.example.demo.validation.SmsCaptchaValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 短信验证码验证
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = SmsCaptchaValidator.class)
public @interface SmsCaptchaCheck {

    String message() default "短信验证码错误或已过期";

    /**
     * 缓存key前缀
     * @return
     */
    String key() ;
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
