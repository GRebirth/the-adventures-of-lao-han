package com.example.demo.validation.annotation;

import com.example.demo.validation.CardElementValidator;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 三四要素认证
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = CardElementValidator.class)
public @interface CardElementCheck {

    String message() default "三四要素认证失败";

    CardElementEnum type();

    /**
     * 额外需校验四要素
     *
     * @return
     */
//    PaymentType[] paymentTypes() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @AllArgsConstructor
    @Getter
    enum CardElementEnum {
        /**
         * 姓名、银行卡号、身份证号
         */
        THREE("三要素"),
        /**
         * 姓名、银行卡号、身份证号、手机号
         */
        FOUR("四要素"),
        /**
         * 姓名、银行卡号、身份证号
         * 手机号（为空走三要素，不为空走四要素）
         */
        THREEORFOUR("三要素或四要素");
        private String name;

    }
}