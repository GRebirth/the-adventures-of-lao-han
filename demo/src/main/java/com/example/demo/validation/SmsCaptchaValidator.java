package com.example.demo.validation;

import cn.hutool.extra.spring.SpringUtil;
import com.example.demo.validation.annotation.SmsCaptchaCheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 短信验证码验证
 */
public class SmsCaptchaValidator implements ConstraintValidator<SmsCaptchaCheck, Object> {
    private SmsCaptchaCheck smsCaptchaCheck;
    @Override
    public void initialize(SmsCaptchaCheck constraintAnnotation) {
       this.smsCaptchaCheck = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        return false;
    }
}
