package com.example.demo.validation;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.extra.spring.SpringUtil;

import com.example.demo.validation.annotation.MobileCheck;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

/**
 * 手机号验证
 */
@Slf4j
public class MobileValidator implements ConstraintValidator<MobileCheck, Object> {

    private MobileCheck mobileCheck;
    @Override
    public void initialize(MobileCheck constraintAnnotation) {
       this.mobileCheck =  constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        String mobile = (String) value;
//        mobile = encrypt(mobile);
        return  true;
//        MerchantInfoService merchantInfoService = SpringUtil.getBean(MerchantInfoService.class);
//        MerchantInfoEntity pmsMerchantInfo = merchantInfoService.getMercInfoByMobile(mobile);
//        if (mobileCheck.isCheckRegistered()) {
//            return BeanUtil.isEmpty(pmsMerchantInfo);
//        }else {
//            return BeanUtil.isNotEmpty(pmsMerchantInfo);
//
//        }
    }

//    public String encrypt(String username) {
//        try {
//            Field field = ReflectUtil.getField(MerchantInfoEntity.class, "mobile");
//            FieldEncrypt fieldEncrypt = field.getAnnotation(FieldEncrypt.class);
//            if (fieldEncrypt != null) {
//                //密钥
//                String key;
//                //私钥
//                String privateKey = CryptoProperties.PRIVATE_KEY;
//                //公钥
//                String publicKey = CryptoProperties.PUBLIC_KEY;
//                //全局配置的key
//                String propertiesKey = CryptoProperties.KEY;
//                //属性上的key
//                String annotationKey = fieldEncrypt.key();
//                if (!"".equals(annotationKey)) {
//                    key = annotationKey;
//                } else {
//                    key = propertiesKey;
//                }
//                Class<? extends ICrypto> aClass = fieldEncrypt.iCrypto();
//                ICrypto crypto = ReflectUtil.newInstance(aClass);
//
//                Algorithm algorithm = fieldEncrypt.algorithm();
//                String encrypt = crypto.encrypt(algorithm, username, privateKey, publicKey, key);
//                return encrypt;
//            }
//        } catch (Exception e) {
//            log.info("加密异常");
//            throw new UsernameNotFoundException("用户" + username + "不可使用请联系管理员!");
//        }
//        return null;
//    }
}
