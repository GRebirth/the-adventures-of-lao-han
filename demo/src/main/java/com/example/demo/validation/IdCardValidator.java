package com.example.demo.validation;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.demo.validation.annotation.IdCardCheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 商户身份证号唯一性验证
 */
public class IdCardValidator implements ConstraintValidator<IdCardCheck, Object> {

    private IdCardCheck idCardCheck;

    @Override
    public void initialize(IdCardCheck constraintAnnotation) {
        this.idCardCheck = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        String idCardNum = (String) value;
        return true;
//        MerchantInfoService merchantInfoService = SpringUtil.getBean(MerchantInfoService.class);
//        MerchantInfoEntity merchantInfo = merchantInfoService.getOne(Wrappers.lambdaQuery(MerchantInfoEntity.class)
//                .eq(MerchantInfoEntity::getIdNumber, idCardNum), false);
//        long merchantId = UserInfoContext.getUser().getUserId();
//        if (idCardCheck.noCheckPayment().equals(PaymentTypeContainer.getPaymentType())) {
//            return true;
//        }
//        if (idCardCheck.isCheckRegistered()) {
//            return ObjectUtil.isNull(merchantInfo) || merchantId == merchantInfo.getId().longValue();
//        } else {
//            return ObjectUtil.isNotNull(merchantInfo);
//        }
    }
}
