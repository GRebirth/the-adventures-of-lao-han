package com.example.demo.validation.annotation;

import com.example.demo.validation.ImageCaptchaValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 图片验证码验证 类上面使用
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ImageCaptchaValidator.class)
public @interface ImageCaptchaCheck {
    String message() default "图形验证码错误或已过期";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
