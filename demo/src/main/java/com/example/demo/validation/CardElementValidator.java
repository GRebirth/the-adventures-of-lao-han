package com.example.demo.validation;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;

import com.example.demo.validation.annotation.CardElementCheck;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CardElementValidator implements ConstraintValidator<CardElementCheck, Object> {

    @Value("${test.card-element-check}")
    private boolean checkTest;


    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (checkTest) {
            return true;
        }
//
//        PaymentType[] paymentTypes = cardElementCheck.paymentTypes();
//        PaymentType paymentType = PaymentTypeContainer.getPaymentType();
//
//        if (ObjectUtil.isNotEmpty(paymentTypes) && ArrayUtil.contains(paymentTypes, paymentType)) {
//            cardElementEnum = CardElementCheck.CardElementEnum.FOUR;
//            setDefaultMessage("四要素认证失败", context);
//        }
//
//        if (value instanceof CardElementCheekParam) {
//            CardElementCheekParam param = (CardElementCheekParam) value;
//            MerchantInfoEntity merchantInfo = SpringUtil.getBean(MerchantInfoService.class).getById(UserInfoContext.getUserId());
//
//            String mobile = StrUtil.isEmpty(param.getMobile()) ? merchantInfo.getMobile(): param.getMobile();
//            String bankCardNumber = param.getBankCardNumber();
//            if ("NONEED".equals(bankCardNumber)) { //对公结算不需要走认证了
//                return true;
//            }
//            String idNumber = StrUtil.isEmpty(param.getIdNumberValid()) ? merchantInfo.getIdNumber() : param.getIdNumberValid();
//            String realName = StrUtil.isEmpty(param.getRealName()) ? merchantInfo.getCorporationName() : param.getRealName();
//            //有参数走取参数的内容，没参数且是开店宝企业走数据库内容
//            if(PaymentType.CANDYPAY.equals(paymentType)
//                    && StrUtil.isEmpty(param.getIdNumberValid())
//                    && StrUtil.isEmpty(param.getRealName())
//                    && MercTypeEnum.ENTERPRISE.getCode()==merchantInfo.getMercType()){ //如果是开店宝企业类型
//                MercCardEntity mercCard = SpringUtil.getBean(MercCardService.class).queryMercDebitCard(merchantInfo.getId());
//                if(mercCard!=null &&mercCard.getIdNumber()==null){ //对公结算直接不走认证
//                    return true;
//                }
//                if(mercCard!=null && mercCard.getName().equals(merchantInfo.getCorporationName())){ //对私法人结算
//                    idNumber=merchantInfo.getIdNumber();
//                    realName=merchantInfo.getCorporationName();
//                }
//                if(mercCard!=null &&mercCard.getIdNumber()!=null&& !merchantInfo.getCorporationName().equals(mercCard.getName())){
//                    //对私非法人结算
//                    idNumber=mercCard.getIdNumber();
//                    realName=mercCard.getName();
//                }
//            }
//            if (CardElementCheck.CardElementEnum.THREE.equals(cardElementEnum)) {
//                return verifyElement(bankCardNumber, realName, idNumber,null, context);
//            } else if (CardElementCheck.CardElementEnum.THREEORFOUR.equals(cardElementEnum)) {
//                //磁条卡认证，无手机号走三要素，有手机号走四要素
//                return verifyElement(bankCardNumber, realName, idNumber, StrUtil.isNotEmpty(param.getMobile()) ? param.getMobile() : null, context);
//            } else {
//                return verifyElement(bankCardNumber, realName, idNumber, mobile, context);
//            }
////            return CardElementCheck.CardElementEnum.THREE.equals(cardElementEnum) ? verifyElement(bankCardNumber, realName, idNumber,null, context) : verifyElement(bankCardNumber, realName, idNumber, mobile, context);
//        }
//        return false;
//    }
//
//    /**
//     * 三四要素
//     * @param bankCardNumber
//     * @param realName
//     * @param idNumber
//     * @param mobile
//     * @param context
//     * @return
//     */
//    private boolean verifyElement(String bankCardNumber, String realName, String idNumber, String mobile, ConstraintValidatorContext context) {
//        InfVerifyApi verifyApi = SpringUtil.getBean(InfVerifyApi.class);
//        R<Void> apiResult = verifyApi.verify(new VerifyParam() {{
//            setBankCardNumber(bankCardNumber);
//            setMobile(mobile);
//            setIdNumber(idNumber);
//            setName(realName);
//        }});
//        if (R.isSuccess(apiResult)){
//            return true;
//        }
//        return false;
//    }

        /**
         * 设置返回校验信息
         *
         * @param defaultMessage
         * @param context
         */
//    public void setDefaultMessage(String defaultMessage, ConstraintValidatorContext context) {
//        context.disableDefaultConstraintViolation();
//        context.buildConstraintViolationWithTemplate(defaultMessage)
//                .addConstraintViolation();
//    }
        return true;
    }
}
