package com.example.demo.validation.annotation;

import com.example.demo.validation.CardUseTypeSoleValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 商户银行卡使用唯一性校验
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = CardUseTypeSoleValidator.class)
public @interface CardUseTypeSoleCheck {
    String message() default "该银行卡已使用";


    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
