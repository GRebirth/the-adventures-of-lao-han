package com.example.demo.validation;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.example.demo.validation.annotation.BankCardCheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

/**
 * 信用卡储蓄卡检查
 *
 */
public class BankCardValidator implements ConstraintValidator<BankCardCheck, Object> {
    private BankCardCheck bankCardCheck;

    @Override
    public void initialize(BankCardCheck constraintAnnotation) {
        bankCardCheck = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        String cardNo = (String) value;
        if (StrUtil.isEmpty(cardNo) || "NONEED".equals(cardNo)) {
            return true;
        }
//        BankCardTypeService bean = SpringUtil.getBean(BankCardTypeService.class);
//        BankCardTypeEntity cardType = bean.getByCardNo(cardNo);
//        String defaultMessage = BeanUtil.isEmpty(cardType) ?
//                StrFormatter.format("银行卡【{}】未识别，请检查卡号是否有误", cardNo)
//                : (bankCardCheck.isCheckDebit() ?
//                // 检查储蓄卡
//                (CardTypeEnum.DEBIT.getCode() == cardType.getCardType() ?
//                        null
//                        : StrFormatter.format("银行卡【{}】为信用卡，请使用储蓄卡", cardNo))
//                // 检查信用卡
//                : (CardTypeEnum.CREDIT.getCode() == cardType.getCardType() ?
//                null
//                : StrFormatter.format("银行卡【{}】为储蓄卡，请使用信用卡", cardNo)));
        return true;
//        return Optional.of(bankCardCheck)
//                .map(BankCardCheck::message)
//                .filter(StrUtil::isBlank)
//                .flatMap(get -> Optional.ofNullable(defaultMessage))
//                .map(message -> {
//                    context.disableDefaultConstraintViolation();
//                    context.buildConstraintViolationWithTemplate(message)
//                            .addConstraintViolation();
//                    return false;})
//                .orElse(true);
    }
}
