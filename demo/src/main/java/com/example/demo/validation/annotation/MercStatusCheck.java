package com.example.demo.validation.annotation;


import com.example.demo.validation.MercStatusValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 登录后使用
 * 商户状态验证
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = MercStatusValidator.class)
public @interface MercStatusCheck {
    String message();

    /**
     * 本业务可进行处理的商户状态
     * @return
     */
//    MercStatusEnum[] mercStatus();

    /**
     * 注解不生效的平台
     */
//    PaymentType[] invalidPayment() default {};

    /**
     * {@link MercStatusCheck#invalidPayment()}生效的商户类型：1-云闪付未绑机商户 0-全部商户生效 <br/>
     * 其他类型的商户需要修改{@link MercStatusValidator}
     */
    int merchantType() default 0;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
